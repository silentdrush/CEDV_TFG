/*************************************************************************************
* This file is part of the Motor Arena project (https://gitlab.com/CEDV19/CEDV_TFG). * 
* Copyright (c) 2019 Javier Osuna Herrera.                                           *
*                                                                                    *
* This program is free software: you can redistribute it and/or modify               *
* it under the terms of the GNU General Public License as published by               *
* the Free Software Foundation, version 3.                                           *
*                                                                                    *
* This program is distributed in the hope that it will be useful, but                *
* WITHOUT ANY WARRANTY; without even the implied warranty of                         *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU                   *
* General Public License for more details.                                           *
*                                                                                    *
* You should have received a copy of the GNU General Public License                  *
* along with this program. If not, see <http://www.gnu.org/licenses/>.               *
**************************************************************************************/

#include "BattleAIController.h"
#include "Actors/Pawns/BorjaMotorcyclePawn.h"
#include "AI/AIActor.h"
#include "AI/EasyAIActor.h"
#include "AI/MediumAIActor.h"
#include "Engine/World.h"

ABattleAIController::ABattleAIController(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	// Generate AIs modes difficulties
	AIModes.Add(TEXT("Easy"), AEasyAIActor::StaticClass());
	AIModes.Add(TEXT("Medium"), AMediumAIActor::StaticClass());

	bReplicates = false;
}

void ABattleAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	
	AISelected->SetPossesedPawnAndExecuteBehavior(Cast<ABorjaMotorcyclePawn>(InPawn));
}

void ABattleAIController::SetAIDifficultyStructure(const ABattleGameState* BattleGameState, const FString AI)
{
	TSubclassOf<AAIActor> AIDesired(AIModes[AI]);
	if (IsValid(AIDesired))
	{
		AISelected = GetWorld()->SpawnActor<AAIActor>(AIDesired);
		AISelected->Initialize(BattleGameState);
	}
}

void ABattleAIController::StopAI()
{
	AISelected->StopTimers();
}
