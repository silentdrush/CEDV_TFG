/*************************************************************************************
* This file is part of the Motor Arena project (https://gitlab.com/CEDV19/CEDV_TFG). * 
* Copyright (c) 2019 Javier Osuna Herrera.                                           *
*                                                                                    *
* This program is free software: you can redistribute it and/or modify               *
* it under the terms of the GNU General Public License as published by               *
* the Free Software Foundation, version 3.                                           *
*                                                                                    *
* This program is distributed in the hope that it will be useful, but                *
* WITHOUT ANY WARRANTY; without even the implied warranty of                         *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU                   *
* General Public License for more details.                                           *
*                                                                                    *
* You should have received a copy of the GNU General Public License                  *
* along with this program. If not, see <http://www.gnu.org/licenses/>.               *
**************************************************************************************/

#include "BattlePlayerController.h"
#include "GameStates/BattleGameState.h"
#include "Blueprint/UserWidget.h"
#include "ConstructorHelpers.h"
#include "Engine/World.h"

ABattlePlayerController::ABattlePlayerController(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	// Get Classes of the menus to create
	UserUIClass = GetMenuAssets("/Game/UI/Battle/W_BattleData");
	FinishUIClass = GetMenuAssets("/Game/UI/Menus/W_GameFinished");
}

void ABattlePlayerController::SetVieportToGame()
{
	CreateMenus();

	// Set properties of controller to use it interacting only with the game
	FInputModeGameOnly Mode;
	SetInputMode(Mode);
	bShowMouseCursor = false;
	UserUI->AddToViewport();
}

void ABattlePlayerController::CreateMenus()
{
	UserUI = CreateMenu(UserUIClass);
	FinishUI = CreateMenu(FinishUIClass);
}

void ABattlePlayerController::SetFinishMenu_Implementation()
{
	if ((IsLocalPlayerController() && 
		(GetWorld()->GetNetMode() == NM_ListenServer || 
			GetWorld()->GetNetMode() == NM_DedicatedServer || 
			GetWorld()->GetNetMode() == NM_Standalone) ||
		GetWorld()->GetNetMode() == NM_Client
	))
	{
		// Set properties of controller to use it interacting only with the menus
		UserUI->RemoveFromViewport();
		FInputModeUIOnly Mode;
		Mode.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);
		SetInputMode(Mode);
		bShowMouseCursor = true;
		if(!FinishUI.IsValid()) FinishUI = CreateMenu(FinishUIClass);
		FinishUI->AddToViewport();
	}
}

bool ABattlePlayerController::SetFinishMenu_Validate()
{
	return true;
}

void ABattlePlayerController::BeginPlay()
{
	Super::BeginPlay();

	if (IsLocalPlayerController() && 
		(GetWorld()->GetNetMode() == NM_ListenServer || 
			GetWorld()->GetNetMode() == NM_DedicatedServer || 
			GetWorld()->GetNetMode() == NM_Standalone
	))
	{
		SetVieportToGame();
	}
	else if (GetWorld()->GetNetMode() == NM_Client)
		SetVieportToGame();
}

TSubclassOf<UUserWidget> ABattlePlayerController::GetMenuAssets(const FString MenuAssetPath) const
{
	auto MenuAssetClass = ConstructorHelpers::FClassFinder<UUserWidget>(*MenuAssetPath);
	if (MenuAssetClass.Succeeded()) return MenuAssetClass.Class;
	UE_LOG(LogTemp, Warning, TEXT("Widget menu %s not found!"), *MenuAssetPath);
	return nullptr;
}

UUserWidget* ABattlePlayerController::CreateMenu(TSubclassOf<UUserWidget> WidgetMenu)
{
	if (IsValid(WidgetMenu)) return CreateWidget<UUserWidget>(this, WidgetMenu);
	UE_LOG(LogTemp, Fatal, TEXT("Widget menu class %s not exist"), *WidgetMenu->GetName());
	return nullptr;
}
