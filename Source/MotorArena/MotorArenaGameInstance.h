/*************************************************************************************
* This file is part of the Motor Arena project (https://gitlab.com/CEDV19/CEDV_TFG). * 
* Copyright (c) 2019 Javier Osuna Herrera.                                           *
*                                                                                    *
* This program is free software: you can redistribute it and/or modify               *
* it under the terms of the GNU General Public License as published by               *
* the Free Software Foundation, version 3.                                           *
*                                                                                    *
* This program is distributed in the hope that it will be useful, but                *
* WITHOUT ANY WARRANTY; without even the implied warranty of                         *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU                   *
* General Public License for more details.                                           *
*                                                                                    *
* You should have received a copy of the GNU General Public License                  *
* along with this program. If not, see <http://www.gnu.org/licenses/>.               *
**************************************************************************************/

#pragma once

#include "Engine/GameInstance.h"
#include "OnlineSubsystemUtils.h"
#include "MotorArenaGameInstance.generated.h"

#define SETTING_SESSION_NAME FName(TEXT("SESSIONNAME"))

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnSessionsFound, const TArray<FString>&, SessionsNames, const TArray<FString>&, SessionsOwningUsername);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnSessionsNotFound);

/**
 * The Game Instance of this game. Contains all multiplayer logic and game globals properties
 */
UCLASS()
class MOTORARENA_API UMotorArenaGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:

	UMotorArenaGameInstance(const FObjectInitializer& ObjectInitializer);

	//Multiplayer public methods
	UFUNCTION(BlueprintCallable, Category = "Network")
		void StartOnlineGame(FString LevelToTravel, FName SessionName = "", bool bIsLAN = true, bool bIsPresence = true, int32 MaxNumPlayers = 4);

	UFUNCTION(BlueprintCallable, Category = "Network")
		void FindOnlineGames(bool bIsLAN = true, bool bIsPresence = true);

	UFUNCTION(BlueprintCallable, Category = "Network")
		void JoinOnlineGame(FString SessionOwningUserName);

	UFUNCTION(BlueprintCallable, Category = "Network")
		void JoinFirstOnlineGame();

	UFUNCTION(BlueprintCallable, Category = "Network")
		void DestroySessionAndLeaveGame();

	UFUNCTION(BlueprintCallable, Category = "Network")
		void ServerTravel(FString LevelToTravel);

	// Multiplayer public delegates
	UPROPERTY(BlueprintAssignable, Category = "Network")
		FOnSessionsFound OnSessionsFoundDo;

	UPROPERTY(BlueprintAssignable, Category = "Network")
		FOnSessionsNotFound OnSessionsNotFoundDo;

	// Global game parameters
	UPROPERTY(BlueprintReadWrite, Category = "Battle")
		int AINumber;

	UPROPERTY(BlueprintReadWrite, Category = "Battle")
		FString BattleDifficulty;

	UPROPERTY(BlueprintReadWrite, Category = "Battle")
		float BattleSpeed;

	UPROPERTY(BlueprintReadWrite, Category = "Battle")
		float OptionsVolume;

protected:

	// Multiplayer methods
	bool HostSession(TSharedPtr<const FUniqueNetId> UserId, FName SessionName, bool bIsLAN, bool bIsPresence, int32 MaxNumPlayers, FString LevelToTravel);
	void FindSessions(TSharedPtr<const FUniqueNetId> UserId, bool bIsLAN, bool bIsPresence);
	bool JoinSession(TSharedPtr<const FUniqueNetId> UserId, FName SessionName, const FOnlineSessionSearchResult& SearchResult);

	// multiplayer delegates methods
	virtual void OnDestroySessionComplete(FName SessionName, bool bWasSuccessful);
	virtual void OnCreateSessionComplete(FName SessionName, bool bWasSuccessful);
	void OnStartOnlineGameComplete(FName SessionName, bool bWasSuccessful);
	void OnFindSessionsComplete(bool bWasSuccessful);
	void OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result);

	// Multiplayer delegates
	FOnCreateSessionCompleteDelegate OnCreateSessionCompleteDelegate;
	FOnStartSessionCompleteDelegate OnStartSessionCompleteDelegate;
	FOnFindSessionsCompleteDelegate OnFindSessionsCompleteDelegate;
	FOnJoinSessionCompleteDelegate OnJoinSessionCompleteDelegate;
	FOnDestroySessionCompleteDelegate OnDestroySessionCompleteDelegate;

	// Register multiplayer delegates
	FDelegateHandle OnCreateSessionCompleteDelegateHandle;
	FDelegateHandle OnStartSessionCompleteDelegateHandle;
	FDelegateHandle OnFindSessionsCompleteDelegateHandle;
	FDelegateHandle OnJoinSessionCompleteDelegateHandle;
	FDelegateHandle OnDestroySessionCompleteDelegateHandle;

	// Multiplayer parameters
	TSharedPtr<class FOnlineSessionSettings> SessionSettings;
	TSharedPtr<class FOnlineSessionSearch> SessionSearch;
};
