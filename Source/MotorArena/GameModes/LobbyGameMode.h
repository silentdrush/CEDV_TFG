/*************************************************************************************
* This file is part of the Motor Arena project (https://gitlab.com/CEDV19/CEDV_TFG). * 
* Copyright (c) 2019 Javier Osuna Herrera.                                           *
*                                                                                    *
* This program is free software: you can redistribute it and/or modify               *
* it under the terms of the GNU General Public License as published by               *
* the Free Software Foundation, version 3.                                           *
*                                                                                    *
* This program is distributed in the hope that it will be useful, but                *
* WITHOUT ANY WARRANTY; without even the implied warranty of                         *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU                   *
* General Public License for more details.                                           *
*                                                                                    *
* You should have received a copy of the GNU General Public License                  *
* along with this program. If not, see <http://www.gnu.org/licenses/>.               *
**************************************************************************************/

#pragma once

#include "GameFramework/GameModeBase.h"
#include "Net/UnrealNetwork.h"
#include "LobbyGameMode.generated.h"

/**
 * This Game Mode is responsible for the lobby configuration and clients login 
 */
UCLASS()
class MOTORARENA_API ALobbyGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	ALobbyGameMode();

protected:

	// Clients login and logout
	void PostLogin(APlayerController* NewPlayer) override;
	void Logout(AController* Exiting) override;

	TWeakObjectPtr<class ALobbyGameState> LobbyGameState;
};
