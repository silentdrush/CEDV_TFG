/*************************************************************************************
* This file is part of the Motor Arena project (https://gitlab.com/CEDV19/CEDV_TFG). * 
* Copyright (c) 2019 Javier Osuna Herrera.                                           *
*                                                                                    *
* This program is free software: you can redistribute it and/or modify               *
* it under the terms of the GNU General Public License as published by               *
* the Free Software Foundation, version 3.                                           *
*                                                                                    *
* This program is distributed in the hope that it will be useful, but                *
* WITHOUT ANY WARRANTY; without even the implied warranty of                         *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU                   *
* General Public License for more details.                                           *
*                                                                                    *
* You should have received a copy of the GNU General Public License                  *
* along with this program. If not, see <http://www.gnu.org/licenses/>.               *
**************************************************************************************/

#include "BattleGameMode.h"
#include "PlayerControllers/BattlePlayerController.h"
#include "MotorArenaGameInstance.h"
#include "Materials/MaterialInstance.h"
#include "GameStates/BattleGameState.h"
#include "AIControllers/BattleAIController.h"
#include "Functionalities.h"
#include "ConstructorHelpers.h"

ABattleGameMode::ABattleGameMode() : Super()
{
	// Default level classes
	PlayerControllerClass = ABattlePlayerController::StaticClass();
	GameStateClass = ABattleGameState::StaticClass();

	// Initialize class variables
	CurrentColorIndex = 0;

	// Initialize colors in motorcycles
	SetMotorcycleColors();
	UFunctionalities::ShuffleArray(MotorcycleColors);
}

void ABattleGameMode::SetMotorcycleColors()
{
	auto ColorBlue = ConstructorHelpers::FObjectFinder<UMaterialInstance>(TEXT("UMaterial'/Game/Materials/CharactersColors/MI_ColorBlue'"));
	if (ColorBlue.Succeeded())
		MotorcycleColors.Add(ColorBlue.Object);

	auto ColorGreen = ConstructorHelpers::FObjectFinder<UMaterialInstance>(TEXT("UMaterial'/Game/Materials/CharactersColors/MI_ColorGreen'"));
	if (ColorGreen.Succeeded())
		MotorcycleColors.Add(ColorGreen.Object);

	auto ColorOrange = ConstructorHelpers::FObjectFinder<UMaterialInstance>(TEXT("UMaterial'/Game/Materials/CharactersColors/MI_ColorOrange'"));
	if (ColorOrange.Succeeded())
		MotorcycleColors.Add(ColorOrange.Object);

	auto ColorPink = ConstructorHelpers::FObjectFinder<UMaterialInstance>(TEXT("UMaterial'/Game/Materials/CharactersColors/MI_ColorPink'"));
	if (ColorPink.Succeeded())
		MotorcycleColors.Add(ColorPink.Object);

	auto ColorPurple = ConstructorHelpers::FObjectFinder<UMaterialInstance>(TEXT("UMaterial'/Game/Materials/CharactersColors/MI_ColorPurple'"));
	if (ColorPurple.Succeeded())
		MotorcycleColors.Add(ColorPurple.Object);

	auto ColorRed = ConstructorHelpers::FObjectFinder<UMaterialInstance>(TEXT("UMaterial'/Game/Materials/CharactersColors/MI_ColorRed'"));
	if (ColorRed.Succeeded())
		MotorcycleColors.Add(ColorRed.Object);

	auto ColorWhite = ConstructorHelpers::FObjectFinder<UMaterialInstance>(TEXT("UMaterial'/Game/Materials/CharactersColors/MI_ColorWhite'"));
	if (ColorWhite.Succeeded())
		MotorcycleColors.Add(ColorWhite.Object);

	auto ColorYellow = ConstructorHelpers::FObjectFinder<UMaterialInstance>(TEXT("UMaterial'/Game/Materials/CharactersColors/MI_ColorYellow'"));
	if (ColorYellow.Succeeded())
		MotorcycleColors.Add(ColorYellow.Object);
}

UMaterialInstance* ABattleGameMode::GetRandomColor()
{
	UMaterialInstance* Color = UFunctionalities::GetRandomElementInArray(MotorcycleColors);
	MotorcycleColors.Remove(Color);
	++CurrentColorIndex;
	return Color;
}

void ABattleGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);
	
	if (!BattleGameState.IsValid()) BattleGameState = Cast<ABattleGameState>(GameState);
	TWeakObjectPtr<ABattlePlayerController> BattlePlayerController = Cast<ABattlePlayerController>(NewPlayer);
	BattleGameState->AddPlayerInGame(BattlePlayerController.Get(), GetRandomColor());
}

void ABattleGameMode::Logout(AController* Exiting)
{
	Super::Logout(Exiting);
	
	BattleGameState->RemovePlayerInGame(Cast<ABattlePlayerController>(Exiting));
}

void ABattleGameMode::BeginPlay()
{
	Super::BeginPlay();

	if (!BattleGameState.IsValid()) BattleGameState = Cast<ABattleGameState>(GameState);
	CreateAIs();
	BattleGameState->StartGame();
}

void ABattleGameMode::CreateAIs()
{
	TWeakObjectPtr<UMotorArenaGameInstance> MotorArenaGameInstance = GetGameInstance<UMotorArenaGameInstance>();
	TWeakObjectPtr<ABattleAIController> AIBattleController;
	for(uint8 index = 0; index < MotorArenaGameInstance->AINumber; ++index)
	{
		AIBattleController = GetWorld()->SpawnActor<ABattleAIController>();
		AIBattleController->SetAIDifficultyStructure(BattleGameState.Get(), MotorArenaGameInstance->BattleDifficulty);
		BattleGameState->AddAIInGame(AIBattleController.Get(), GetRandomColor());
	}
}
