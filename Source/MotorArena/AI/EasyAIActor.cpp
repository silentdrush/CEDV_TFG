/*************************************************************************************
* This file is part of the Motor Arena project (https://gitlab.com/CEDV19/CEDV_TFG). * 
* Copyright (c) 2019 Javier Osuna Herrera.                                           *
*                                                                                    *
* This program is free software: you can redistribute it and/or modify               *
* it under the terms of the GNU General Public License as published by               *
* the Free Software Foundation, version 3.                                           *
*                                                                                    *
* This program is distributed in the hope that it will be useful, but                *
* WITHOUT ANY WARRANTY; without even the implied warranty of                         *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU                   *
* General Public License for more details.                                           *
*                                                                                    *
* You should have received a copy of the GNU General Public License                  *
* along with this program. If not, see <http://www.gnu.org/licenses/>.               *
**************************************************************************************/

#include "EasyAIActor.h"
#include "Actors/Pawns/BorjaMotorcyclePawn.h"
#include "Engine.h"

AEasyAIActor::AEasyAIActor() : Super()
{
	// Initialize class variables
	bResetMovement = true;
	NumberOfTimesToMove = 1;
}

void AEasyAIActor::Behavior()
{
	Super::Behavior();
	
	if (PossesedPawn.IsValid())
	{
		if (bResetMovement)
		{
			bResetMovement = false;
			NumberOfTimesToMove = FMath::RandRange(1, 20);
		}
		else
		{
			--NumberOfTimesToMove;
			if (NumberOfTimesToMove == 0)
			{
				bResetMovement = true;
				const FVector NewDirection = FMath::RandBool() ? PossesedPawn->GetActorRightVector() * -1 : PossesedPawn->GetActorRightVector();
				LateralsPrecautionsMovement(NewDirection);
			}
		}
	}
}
