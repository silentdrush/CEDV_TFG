/*************************************************************************************
* This file is part of the Motor Arena project (https://gitlab.com/CEDV19/CEDV_TFG). * 
* Copyright (c) 2019 Javier Osuna Herrera.                                           *
*                                                                                    *
* This program is free software: you can redistribute it and/or modify               *
* it under the terms of the GNU General Public License as published by               *
* the Free Software Foundation, version 3.                                           *
*                                                                                    *
* This program is distributed in the hope that it will be useful, but                *
* WITHOUT ANY WARRANTY; without even the implied warranty of                         *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU                   *
* General Public License for more details.                                           *
*                                                                                    *
* You should have received a copy of the GNU General Public License                  *
* along with this program. If not, see <http://www.gnu.org/licenses/>.               *
**************************************************************************************/

#include "AIActor.h"
#include "Actors/Pawns/BorjaMotorcyclePawn.h"
#include "GameStates/BattleGameState.h"
#include "Engine/World.h"

AAIActor::AAIActor() : Super()
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = false;

	// Class variables
	TimeToWaitToExecuteBehavior = 1.f;
	TimeToWaitToExecuteBehaviorFirstTime = 0.f;
	bExecuteBehaviorInLoop = true;
	TimeToWaitToExecuteCollisionDetections = 0.05f;
	TimeToWaitToExecuteCollisionsDetectionFirstTime = 0.f;
	bExecuteColisionsDetectionInLoop = true;
	bCanExexuteBehavior = false;
}

void AAIActor::Initialize(
	const ABattleGameState* GameState,
	const ABorjaMotorcyclePawn* Pawn)
{
	BattleGameState = GameState;
	if (IsValid(Pawn)) SetPossesedPawnAndExecuteBehavior(Pawn);
}

void AAIActor::SetPossesedPawnAndExecuteBehavior(const ABorjaMotorcyclePawn* Pawn)
{
	PossesedPawn = Pawn;
	ExecuteBehavior();
}

void AAIActor::ExecuteBehavior()
{
	// Set class variables
	bCanExexuteBehavior = true;
}

void AAIActor::StopTimers()
{
	GetWorld()->GetTimerManager().ClearTimer(CollisionsTimer);
	GetWorld()->GetTimerManager().ClearTimer(MovementTimer);
	if (PossesedPawn.IsValid()) PossesedPawn->FinishGame();
}

void AAIActor::BeginPlay()
{
	Super::BeginPlay();

	MovementTimerCallback.BindLambda([this] { if (bCanExexuteBehavior) Behavior(); });
	GetWorld()->GetTimerManager().SetTimer(
		MovementTimer, 
		MovementTimerCallback,
		TimeToWaitToExecuteBehavior, 
		bExecuteBehaviorInLoop, 
		TimeToWaitToExecuteBehaviorFirstTime
	);

	CollisionsTimerCallback.BindLambda([this] { if (bCanExexuteBehavior) CollisionDetection(); });
	GetWorld()->GetTimerManager().SetTimer(
		CollisionsTimer,
		CollisionsTimerCallback,
		TimeToWaitToExecuteCollisionDetections,
		bExecuteColisionsDetectionInLoop,
		TimeToWaitToExecuteCollisionsDetectionFirstTime
	);
}

void AAIActor::Behavior()
{
	if (!PossesedPawn.IsValid())
		StopTimers();

	// TODO: Implements by its children to do AI behavior
}

void AAIActor::CollisionDetection()
{
	if (PossesedPawn.IsValid())
	{
		FHitResult OutHit;
		FVector Start = (PossesedPawn->GetActorForwardVector() * 320.f) + PossesedPawn->GetActorLocation();
		FVector End = (PossesedPawn->GetActorForwardVector() * 350.f) + PossesedPawn->GetActorLocation();
		Start.Z += 50.f;
		End.Z += 50.f;

		bool bCollioned = GetWorld()->LineTraceSingleByObjectType(
			OutHit,
			Start,
			End,
			ECC_WorldDynamic
		);

		if (bCollioned)
		{
			WideMovement();
			FordwardCollisionDetection();
		}
	}
}

void AAIActor::FordwardCollisionDetection()
{
	// TODO: Implements by its children to do AI prevent fordward collision
}

void AAIActor::LateralsPrecautionsMovement(const FVector NewForwardDirection)
{
	if (PossesedPawn.IsValid())
	{
		FHitResult OutHitRight;
		FHitResult OutHitLeft;
		FVector StartRight = (PossesedPawn->GetActorRightVector() * 100.f) + PossesedPawn->GetActorLocation();
		FVector StartLeft = (PossesedPawn->GetActorRightVector() * 100.f * -1.f) + PossesedPawn->GetActorLocation();
		FVector EndRight = (PossesedPawn->GetActorRightVector() * 500.f) + PossesedPawn->GetActorLocation();
		FVector EndLeft = (PossesedPawn->GetActorRightVector() * 500.f * -1.f) + PossesedPawn->GetActorLocation();
		StartRight.Z += 30.f;
		StartLeft.Z += 30.f;
		StartRight.X += -10.f;
		StartLeft.X += -10.f;
		EndRight.Z += 30.f;
		EndLeft.Z += 30.f;
		EndRight.X += -10.f;
		EndLeft.X += -10.f;

		// Calculate collission in both 
		bool bCollionedRight = GetWorld()->LineTraceSingleByObjectType(
			OutHitRight,
			StartRight,
			EndRight,
			ECC_WorldDynamic
		);
		bool bCollionedLeft = GetWorld()->LineTraceSingleByObjectType(
			OutHitLeft,
			StartLeft,
			EndLeft,
			ECC_WorldDynamic
		);

		// Check the direction to move
		const float DotProductNewDirectionRight = FVector::DotProduct(PossesedPawn->GetActorRightVector(), NewForwardDirection);
		const float DotProductNewDirectionLeft = FVector::DotProduct(PossesedPawn->GetActorRightVector() * -1, NewForwardDirection);
		const float DotProductForwardDirection = FVector::DotProduct(PossesedPawn->GetActorForwardVector(), NewForwardDirection);

		if (DotProductNewDirectionRight > DotProductForwardDirection &&
			DotProductForwardDirection > DotProductNewDirectionLeft &&
			!bCollionedRight
			)
		{
			PossesedPawn->MoveRight();
		}
		if (DotProductNewDirectionLeft > DotProductForwardDirection &&
			DotProductForwardDirection > DotProductNewDirectionRight &&
			!bCollionedLeft
			)
		{
			PossesedPawn->MoveLeft();
		}
	}
}

void AAIActor::WideMovement()
{
	if (PossesedPawn.IsValid())
	{
		// Generate Variables to detect the collision
		FHitResult OutHitRight;
		FHitResult OutHitLeft;
		FVector StartRight = (PossesedPawn->GetActorRightVector() * 100.f) + PossesedPawn->GetActorLocation();
		FVector StartLeft = (PossesedPawn->GetActorRightVector() * 100.f * -1.f) + PossesedPawn->GetActorLocation();
		FVector EndRight = (PossesedPawn->GetActorRightVector() * 50000.f) + PossesedPawn->GetActorLocation();
		FVector EndLeft = (PossesedPawn->GetActorRightVector() * 50000.f * -1.f) + PossesedPawn->GetActorLocation();
		StartRight.Z += 30.f;
		StartLeft.Z += 30.f;
		StartRight.X += -10.f;
		StartLeft.X += -10.f;
		EndRight.Z += 30.f;
		EndLeft.Z += 30.f;
		EndRight.X += -10.f;
		EndLeft.X += -10.f;

		// Calculate collission in both sides 
		bool bCollionedRight = GetWorld()->LineTraceSingleByObjectType(
			OutHitRight,
			StartRight,
			EndRight,
			ECC_WorldDynamic
		);
		bool bCollionedLeft = GetWorld()->LineTraceSingleByObjectType(
			OutHitLeft,
			StartLeft,
			EndLeft,
			ECC_WorldDynamic
		);

		// Determine the most wide movement
		if (bCollionedLeft && bCollionedRight)
		{
			if (FVector::Dist(OutHitRight.GetActor()->GetActorLocation(), PossesedPawn->GetActorLocation()) <
				FVector::Dist(OutHitLeft.GetActor()->GetActorLocation(), PossesedPawn->GetActorLocation()))
			{
				PossesedPawn->MoveLeft();
			}
			else PossesedPawn->MoveRight();
		}
		else if (bCollionedRight && !bCollionedLeft)
			PossesedPawn->MoveLeft();
		else if (!bCollionedRight && bCollionedLeft)
			PossesedPawn->MoveRight();
		else
			FMath::RandBool() ? PossesedPawn->MoveRight() : PossesedPawn->MoveLeft();
	}
}
