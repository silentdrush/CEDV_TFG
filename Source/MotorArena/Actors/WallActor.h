/*************************************************************************************
* This file is part of the Motor Arena project (https://gitlab.com/CEDV19/CEDV_TFG). * 
* Copyright (c) 2019 Javier Osuna Herrera.                                           *
*                                                                                    *
* This program is free software: you can redistribute it and/or modify               *
* it under the terms of the GNU General Public License as published by               *
* the Free Software Foundation, version 3.                                           *
*                                                                                    *
* This program is distributed in the hope that it will be useful, but                *
* WITHOUT ANY WARRANTY; without even the implied warranty of                         *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU                   *
* General Public License for more details.                                           *
*                                                                                    *
* You should have received a copy of the GNU General Public License                  *
* along with this program. If not, see <http://www.gnu.org/licenses/>.               *
**************************************************************************************/

#pragma once

#include "GameFramework/Actor.h"
#include "Net/UnrealNetwork.h"
#include "WallActor.generated.h"

/**
 * The wall actor instantiated by the game characters
 */
UCLASS()
class MOTORARENA_API AWallActor : public AActor 
{
	GENERATED_BODY()
	
public:

	explicit AWallActor(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintCallable, Category = "Wall")
		void Initialize(
			const class AActor* CreatedActor,
			FVector Location, 
			FRotator Rotation, 
			class UMaterialInstance* WallColor = nullptr,
			float TopPadding = 0.f, 
			float Width = 0.1f, 
			float Height = 1
		);

	UFUNCTION(BlueprintCallable, Category = "Wall")
		FVector Finalize(FVector EndLocation, float EndPadding);

	UFUNCTION(BlueprintCallable, Category = "Wall")
		void SetLocationAndRotation(FVector NewLocation, FRotator NewRotation);

	UFUNCTION(BlueprintCallable, Category = "Wall")
		void SetWallLength(FVector NewLocation);

	UFUNCTION(BlueprintCallable, Category = "Wall")
		void SetColor();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Wall)
		TWeakObjectPtr<class AActor> CreatedActor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Wall)
		FVector SpawnLocation;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Wall)
		float TopPadding;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Wall)
		float ConversionValueMeshScale;

protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* Mesh;

	TWeakObjectPtr<class UStaticMesh> WallMesh;

	UPROPERTY(ReplicatedUsing=SetColor)
		TWeakObjectPtr<class UMaterialInstance> WallMaterial;

	FVector Scale;
	float DistanceDifference;
};
