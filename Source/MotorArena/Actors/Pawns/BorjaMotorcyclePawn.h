/*************************************************************************************
* This file is part of the Motor Arena project (https://gitlab.com/CEDV19/CEDV_TFG). * 
* Copyright (c) 2019 Javier Osuna Herrera.                                           *
*                                                                                    *
* This program is free software: you can redistribute it and/or modify               *
* it under the terms of the GNU General Public License as published by               *
* the Free Software Foundation, version 3.                                           *
*                                                                                    *
* This program is distributed in the hope that it will be useful, but                *
* WITHOUT ANY WARRANTY; without even the implied warranty of                         *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU                   *
* General Public License for more details.                                           *
*                                                                                    *
* You should have received a copy of the GNU General Public License                  *
* along with this program. If not, see <http://www.gnu.org/licenses/>.               *
**************************************************************************************/

#pragma once

#include "CoreMinimal.h"
#include "Net/UnrealNetwork.h"
#include "GameFramework/Pawn.h"
#include "BorjaMotorcyclePawn.generated.h"

/**
 * The struct which represent the movement and direction of ABorjaMotorcyclePawn
 */
USTRUCT(BlueprintType)
struct MOTORARENA_API FDirection
{
	GENERATED_USTRUCT_BODY()

	FDirection();
	explicit FDirection(const TArray<FVector>& Directions, uint8 StartDirectionInDirections = 0);

	void SetDirections(const TArray<FVector>& Directions, uint8 StartDirectionInDirections = 0);
	void SetRotations(const TArray<FRotator>& Rotations, FVector ForwardDirection, uint8 StartDirectionInDirections = 0);
	bool Equals(FDirection& OtherDirection);
	FVector MoveDirection(FDirection& OtherDirection);
	FVector GetDirection();
	FRotator GetRotation();
	FVector MoveRightDirection();
	FVector MoveLeftDirection();
	FVector SetDirection();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = CharacterDirection)
		TArray<FVector> Directions;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = CharacterDirection)
		TArray<FRotator> Rotations;

	UPROPERTY()
		int8 CurrentDirection;

private:

	int8 NormalizeCurrentDirectionIndex(int8 Index);
	FVector MoveDirection();
};

/**
 * The main character in the game which generate the walls and its possesed by the AI and the clients
 */
UCLASS()
class MOTORARENA_API ABorjaMotorcyclePawn : public APawn
{
	GENERATED_BODY()

public:

	ABorjaMotorcyclePawn(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintCallable, Category = "BorjaMotorcylePawn")
		void Initialize(FVector Location, FVector FordwardVector, class UMaterialInstance* PlayerColor, class ABattleGameState* GameState);

	UFUNCTION(BlueprintCallable, Category = "Movement")
		void MoveRight();

	UFUNCTION(BlueprintCallable, Category = "Movement")
		void MoveLeft();

	UFUNCTION(BlueprintCallable, Category = "BorjaMotorcylePawn")
		void EliminateInGame();

	UFUNCTION(BlueprintCallable, Category = "BorjaMotorcylePawn")
		void StartGame();

	UFUNCTION(BlueprintCallable, Category = "BorjaMotorcylePawn")
		bool IsDead();

	UFUNCTION(BlueprintCallable, Category = "BorjaMotorcylePawn")
		void FinishGame();

	UFUNCTION(BlueprintCallable, Category = "BorjaMotorcylePawn")
		FVector MoveDirection(ABorjaMotorcyclePawn* OtherPawn);

	UPROPERTY(Replicated)
		FDirection CurrentDirection;

	UPROPERTY(ReplicatedUsing=SetBorjaMaterials)
		TWeakObjectPtr<UMaterialInstance> CurrentColor;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseTurnRate;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseLookUpRate;

protected:

	void Tick(float DeltaSeconds) override;
	void BeginPlay() override;

	UFUNCTION()
		void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION(Server, Reliable, WithValidation)
		void LateralMoveInServer(bool Right);

	UFUNCTION()
		void SetBorjaMaterials();

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void TurnAtRate(float Rate);
	void LookUpAtRate(float Rate);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "BorjaMotorcyclePawn")
		bool bIsDead;

	UPROPERTY(VisibleAnywhere, Replicated, BlueprintReadOnly, Category = "BorjaMotorcyclePawn")
		bool bIsGamePlaying;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "BorjaMotorcyclePawn")
		TWeakObjectPtr<class ABattleGameState> BattleGameState;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "BorjaMotorcyclePawn")
		TWeakObjectPtr<class UMotorArenaGameInstance> MotorArenaGameInstance;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = WallGenerator, meta = (AllowPrivateAccess = "true"))
		class UWallGeneratorActorComponent* WallGenerator;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USceneComponent* SceneRoot;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* MotorcycleMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USkeletalMeshComponent* BorjaMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UBoxComponent* CoreTrigger;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* FollowCamera;
};
