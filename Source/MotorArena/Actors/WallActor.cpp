/*************************************************************************************
* This file is part of the Motor Arena project (https://gitlab.com/CEDV19/CEDV_TFG). * 
* Copyright (c) 2019 Javier Osuna Herrera.                                           *
*                                                                                    *
* This program is free software: you can redistribute it and/or modify               *
* it under the terms of the GNU General Public License as published by               *
* the Free Software Foundation, version 3.                                           *
*                                                                                    *
* This program is distributed in the hope that it will be useful, but                *
* WITHOUT ANY WARRANTY; without even the implied warranty of                         *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU                   *
* General Public License for more details.                                           *
*                                                                                    *
* You should have received a copy of the GNU General Public License                  *
* along with this program. If not, see <http://www.gnu.org/licenses/>.               *
**************************************************************************************/

#include "WallActor.h"
#include "Engine/World.h"
#include "Components/StaticMeshComponent.h"
#include "ConstructorHelpers.h"
#include "GameFramework/MovementComponent.h"
#include "Materials/MaterialInstance.h"

AWallActor::AWallActor(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	// Set Static mesh
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	RootComponent = Mesh;
	Mesh->SetCollisionProfileName(TEXT("Trigger"));

	// Get Cube Mesh
	const auto MeshAsset = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("UStaticMesh'/Game/Models/WallCube/SM_WallCube'"));
	if(MeshAsset.Succeeded())
	{
		WallMesh = MeshAsset.Object;
		Mesh->SetStaticMesh(WallMesh.Get());
	}

	// Initialize class variables
	ConversionValueMeshScale = 200.f;

	// Set Replication to the Wall
	bReplicates = true;
	bReplicateMovement = true;
	Mesh->SetIsReplicated(true);
}

void AWallActor::Initialize(
	const AActor* CreatedActor, 
	const FVector Location, 
	const FRotator Rotation, 
	class UMaterialInstance* WallColor,
	const float TopPadding, 
	const float Width, 
	const float Height 
)
{
	// Initialize class variables
	this->CreatedActor = CreatedActor;
	this->TopPadding = TopPadding;
	SpawnLocation = Location - ((CreatedActor->GetActorForwardVector() * Width * ConversionValueMeshScale) / 2);
	SetActorLocationAndRotation(Location, Rotation);
	Scale = FVector(0.f, Width, Height);
	SetActorScale3D(Scale);
	SetWallLength(Location);
	if (WallColor != nullptr)
	{
		WallMaterial = WallColor;
		SetColor();
	}
}

FVector AWallActor::Finalize(FVector EndLocation, const float EndPadding)
{
	SetActorLocation(EndLocation + (GetActorScale().Y * GetActorForwardVector() * ConversionValueMeshScale * EndPadding));
	SetWallLength(GetActorLocation());
	return GetActorLocation();
}

void AWallActor::SetLocationAndRotation(const FVector NewLocation, const FRotator NewRotation)
{
	SetActorLocationAndRotation(NewLocation, NewRotation);
	SetWallLength(NewLocation);
}

void AWallActor::SetWallLength(const FVector NewLocation)
{
	DistanceDifference = FVector::Dist(NewLocation, SpawnLocation);
	Scale.X = DistanceDifference / ConversionValueMeshScale;
	SetActorScale3D(Scale);
}

void AWallActor::SetColor()
{
	Mesh->SetMaterial(0, WallMaterial.Get());
}

void AWallActor::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AWallActor, WallMaterial);
}
