/*************************************************************************************
* This file is part of the Motor Arena project (https://gitlab.com/CEDV19/CEDV_TFG). * 
* Copyright (c) 2019 Javier Osuna Herrera.                                           *
*                                                                                    *
* This program is free software: you can redistribute it and/or modify               *
* it under the terms of the GNU General Public License as published by               *
* the Free Software Foundation, version 3.                                           *
*                                                                                    *
* This program is distributed in the hope that it will be useful, but                *
* WITHOUT ANY WARRANTY; without even the implied warranty of                         *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU                   *
* General Public License for more details.                                           *
*                                                                                    *
* You should have received a copy of the GNU General Public License                  *
* along with this program. If not, see <http://www.gnu.org/licenses/>.               *
**************************************************************************************/

#include "Functionalities.h"

AActor* UFunctionalities::MinActorDistance(
	const AActor* ActorSource, 
	const TArray<AActor*>& ActorsToCheckDistance,
	float& MinDistance
)
{
	float ElementDistance = MinDistance = TNumericLimits<float>::Max();
	AActor* ActorWithMinimumDistance = nullptr;
	for (uint8 Index = 0; Index < ActorsToCheckDistance.Num(); ++Index)
	{
		if (ActorSource != ActorsToCheckDistance[Index])
		{
			ElementDistance = ActorSource->GetDistanceTo(ActorsToCheckDistance[Index]);
			if (ElementDistance < MinDistance)
			{
				MinDistance = ElementDistance;
				ActorWithMinimumDistance = ActorsToCheckDistance[Index];
			}
		}
	}
	return ActorWithMinimumDistance;
}
